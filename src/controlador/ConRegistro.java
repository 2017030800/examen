package controlador;

import modelo.ModRegistro;
import modelo.Producto;
import vista.ViewRegistro;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

public class ConRegistro implements ActionListener, Runnable{

    private ViewRegistro vista;
    private ModRegistro modelo;

    public ConRegistro(ViewRegistro vista, ModRegistro modelo){
        this.modelo = modelo;
        this.vista = vista;
        this.vista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.vista.setTitle("Extempo");
        cargarVentana();
        this.vista.pack();
        new Thread(this, "reloj").start();
        this.vista.show();
    }

    private void cargarVentana()
    {

        vista.btnAgregar.addActionListener(this);
        vista.btnSalir.addActionListener(this);
        vista.btnBorrar.addActionListener(this);
        vista.btnActualiizar.addActionListener(this);
        vista.btnConsultar.addActionListener(this);
        vista.tbDatos.setModel(modelo.consultar());

    }


    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getSource() == vista.btnAgregar) {
            Producto producto = new Producto();
            producto.setNombre(vista.txtNombre.getText());
            producto.setDescripcion(vista.txtDescripcion.getText());
            producto.setExistencia(Integer.parseInt(vista.txtExistencia.getText()));
            if (modelo.agregar(producto.getNombre(), producto.getDescripcion(), producto.getExistencia())) {
                JOptionPane.showMessageDialog(null, "Se agrego de manera correcta", "Agregar", JOptionPane.OK_OPTION);
            } else {
                JOptionPane.showMessageDialog(null, "Se agrego de manera correcta", "Agregar", JOptionPane.OK_OPTION);
            }
            vista.tbDatos.setModel(modelo.consultar());
        } else if (actionEvent.getSource() == vista.btnActualiizar) {
            Producto producto = new Producto();
            producto.setCodigo(Integer.parseInt(vista.txtCodigo.getText()));
            producto.setNombre(vista.txtNombre.getText());
            producto.setDescripcion(vista.txtDescripcion.getText());
            producto.setExistencia(Integer.parseInt(vista.txtExistencia.getText()));
            if (modelo.actualizar(producto.getCodigo(), producto.getNombre(), producto.getDescripcion(), producto.getExistencia())) {
                JOptionPane.showMessageDialog(null, "Se actualizo de manera correcta", "Agregar", JOptionPane.OK_OPTION);
            } else {
                JOptionPane.showMessageDialog(null, "ERROR", "Agregar", JOptionPane.OK_OPTION);
            }
            vista.tbDatos.setModel(modelo.consultar());
        }
        else if (actionEvent.getSource() == vista.btnBorrar)
        {
            int codigo = Integer.parseInt(vista.txtCodigo.getText());
            if(modelo.borrar(codigo))
            {
                JOptionPane.showMessageDialog(null, "Se elimino correctamente", "Agregar", JOptionPane.OK_OPTION);
            } else {
                JOptionPane.showMessageDialog(null, "ERROR", "Agregar", JOptionPane.OK_OPTION);
            }
            vista.tbDatos.setModel(modelo.consultar());
        }
        else if(actionEvent.getSource() == vista.btnSalir)
        {
            System.exit(0);
        }
        else if(actionEvent.getSource() == vista.btnConsultar)
        {
            int codigo = Integer.parseInt(vista.txtCodigo.getText());
            Producto prod = modelo.consutarProducto(codigo);

            if(prod == null)
            {
                JOptionPane.showMessageDialog(null, "Este producto no existe", "Consultar", JOptionPane.OK_OPTION);

            }
            else
            {
                vista.txtExistencia.setText(String.valueOf(prod.getExistencia()));
                vista.txtDescripcion.setText(prod.getDescripcion());
                vista.txtNombre.setText(prod.getNombre());
            }

        }

    }

    @Override
    public void run() {
        if(Thread.currentThread().getName().equals("reloj"))
        {
            Date now = new Date();
            vista.txtReloj.setText(now.getHours() + ":" +now.getMinutes() + ":" + now.getSeconds());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
