package modelo;

import javax.swing.table.DefaultTableModel;
import java.sql.*;

public class ModRegistro {
    private DBConexion db;

    public ModRegistro()
    {
        this.db = new DBConexion();
    }

    public DefaultTableModel consultar() {
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("Codigo");
        modelo.addColumn("Nombre");
        modelo.addColumn("Descripcion");
        modelo.addColumn("Exitencia");
        String sql = "Select codigo, nombre, descripcion, existencia from registro where status = 1";
        try {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            ResultSet resultado = pred.executeQuery();
            while (resultado.next()) {
                modelo.addRow(new Object[]{
                        resultado.getInt(1),
                        resultado.getString(2),
                        resultado.getString(3),
                        resultado.getInt(4)
                });
            }
            db.closeConection(con);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return modelo;
    }

    public Producto consutarProducto(int codigo)
    {
        Producto prod = null;
        String sql = "Select codigo, nombre, descripcion, existencia from registro where status = 1 and codigo = ?";
        try {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setInt(1, codigo);
            ResultSet resultado = pred.executeQuery();
            while (resultado.next()) {
                prod = new Producto();
                prod.setCodigo(resultado.getInt(1));
                prod.setNombre(resultado.getString(2));
                prod.setDescripcion(resultado.getString(3));
                prod.setExistencia(resultado.getInt(1));
            }
            db.closeConection(con);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return prod;
    }

    public boolean agregar(String nombre, String descripcion, int existencia)
    {
        String sql = "INSERT INTO registro set nombre = ?, descripcion = ?, existencia = ?";
        try
        {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setString(1, nombre);
            pred.setString(2, descripcion);
            pred.setInt(3, existencia);
            pred.executeUpdate();
            db.closeConection(con);
            return true;
        }catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public boolean actualizar(int  codigo, String nombre, String descripcion, int existencia)
    {
        String sql = "UPDATE registro set nombre = ?, descripcion = ?, existencia = ? where codigo = ?";
        try
        {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setString(1, nombre);
            pred.setString(2, descripcion);
            pred.setInt(3, existencia);
            pred.setInt(4, codigo);
            pred.executeUpdate();
            db.closeConection(con);
            return true;
        }catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public boolean borrar(int  codigo)
    {
        String sql = "UPDATE registro set status = 0 where codigo = ?";
        try
        {
            Connection con = db.openConection();
            PreparedStatement pred = con.prepareStatement(sql);
            pred.setInt(1, codigo);
            pred.executeUpdate();
            db.closeConection(con);
            return true;
        }catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }




}
