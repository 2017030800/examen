import controlador.ConRegistro;
import modelo.ModRegistro;
import vista.ViewRegistro;

public class Main {
    public static void main(String[] agrs)
    {
        new ConRegistro(new ViewRegistro(), new ModRegistro());
    }
}
