`extempo`/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 10.4.8-MariaDB : Database - extempo
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`extempoBD` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `extempoBD`;

/*Table structure for table `registro` */

DROP TABLE IF EXISTS `registros`;

CREATE TABLE `registro` (
  `codigo` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(150) DEFAULT NULL,
  `descripcion` VARCHAR(150) DEFAULT NULL,
  `existencia` INT(11) DEFAULT NULL,
  `status` TINYINT(1) DEFAULT 1,
  PRIMARY KEY (`codigo`)
) ENGINE=INNODB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `registro` */

INSERT  INTO `registro`(`codigo`,`nombre`,`descripcion`,`existencia`,`status`) VALUES 
(1,'Marias`extempo``mysql`','',12,0),
(2,'Peras','Manzanas',12,1),
(3,'Chokis','`mysql`',12,1),
(4,'Principes','Tortas',2,1);
`extempo`
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
